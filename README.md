
# Bank My Change

If you are into passive saving and don't mind rounding up your transactions, this is the project for you.




## License

[MIT](https://choosealicense.com/licenses/mit/)


## Requirements
- Free Account with freecurrencyapi 
- Free Telegram bot
- Credit Card, and 2x Savings accounts with Investec (one can be cash management)
- Access to programmable API (open banking api)
## Features

- Round up transactions to the nearest R10 and move the rest to savings
- Always keep your funds in cash management account (or other) and only move them to your cheque account when you need automatically
- Complete serverless using AWS lambda but easily cloud agnostic
- Lambda will only be called when you swipe. so it will cost next to nothing
- Written in python 3




## Authors

- [@muhammadebrahim](https://www.linkedin.com/in/muhammad-ebrahim-60214921/)




## Contributing

Contributions are always welcome!

Please create a pull request and be respectful.


## Documentation

Code is clearly commented and installation instructions below.


## Installation

- Start by creating a new Lambda Function. I typically use Python 3.8 and arm64
- Link a trigger to the lambda function and select API Gateway and select REST with no auth (or you can choose to use auth if you like)
- Zip the repo root folder and upload it as the function
- Fill in the client API, Telegram API, Freecurrenct API and account number details
- Get your card details from the programmable banking IDE and place those in the "allowedCards" dict
- Make sure to add main.js sample code to your programmable card (change the URL in POST to your API Gateway URL)

Thats it. Pretty simple to get going
## Tech Stack

**Serverless:** AWS Lambda (Python 3.8)

