import json
import requests
import math

## Edit these with your details
client_id = "my_investec_client_id"
client_secret = "my_investec_secret"
cashManagementAccount = "account_id_use_postman"
creditcardAccount = "account_id_use_postman"
savingsAccount = "account_id_use_postman"
telegramBotToken = "token_given_by_bot_fathe"
telegramChatID = "my_tg_clientid"
freecurrencyapi="freecurrencyapi_secret"
allowedCards=["get","me","from","programmable","cards"]

    
def lambda_handler(event, context):
    
    ## Get the payload from body and store in variables
    body = json.loads(event["body"])

    amount=int(body['centsAmount'])/100
    amountOrig=amount
    currency=body['currencyCode'].upper()
    currencyOrig=currency
    merch=body['merchant']['name']
   
    if body['card']['id'] not in allowedCards:
        return {
            'statusCode': 401,
            'body': json.dumps('Unauthorized!')
        }
    
    ## Get Bearer Token for open banking API
    url = 'https://openapi.investec.com/identity/v2/oauth2/token'
    headers = {"content-type": "application/x-www-form-urlencoded", "Authorization": "Basic e2NsaWVudElkfTp7c2VjcmV0fQ=="}
    data = "grant_type=client_credentials&scope=accounts"
    tokentext = requests.post(url, headers=headers, data=data, auth=(client_id, client_secret))
    token = json.loads(tokentext.text)
    token = token["access_token"]
    
    ## Convert to ZAR
    if currency != "ZAR":
        url = "https://freecurrencyapi.net/api/v2/latest?apikey="+freecurrencyapi+"&base_currency=ZAR"
        rates = requests.get(url)
        rates=json.loads(rates.text)
        for r in rates["data"]:
            if r == currency:
                amountOrig=amount
                amount=amount*1.025 # Adding currency conversion fees
                amount=round(amount/float(rates["data"][r]),2)
                currencyOrig=currency
                currency="ZAR"
        
    
    ## Make first Transfer to Credit Card
    url = 'https://openapi.investec.com/za/pb/v1/accounts/transfermultiple'
    headers = {"content-type": "application/json", "Authorization": "Bearer "+token}
    data='{"AccountId": '+cashManagementAccount+', "TransferList": [{"BeneficiaryAccountId": '+creditcardAccount+', "MyReference": "Auto Transfer", "TheirReference": "Auto Transfer", "amount": '+str(amount)+'}]}'
    xfer = requests.post(url, headers=headers, data=data)
    
    ## Now to take care of rounding up to nearest R10
    amountrnd = round(math.ceil(amount / 10) * 10 - amount,2)
    
    ## Make Second Transfer to Savings
    url = 'https://openapi.investec.com/za/pb/v1/accounts/transfermultiple'
    headers = {"content-type": "application/json", "Authorization": "Bearer "+token}
    data='{"AccountId": '+cashManagementAccount+', "TransferList": [{"BeneficiaryAccountId": '+savingsAccount+', "MyReference": "Auto Transfer", "TheirReference": "Auto Transfer", "amount": '+str(amountrnd)+'}]}'
    xfer = requests.post(url, headers=headers, data=data)
    
    ## Send out message summary of transactions
    bot_message=str(amountOrig) + "" + str(currencyOrig) + " Reserved for purchase at " + str(merch)+"\n"+str(amount) + "" + str(currency)+ " Transferred into Credit Card\n"+str(amountrnd) + "" + str(currency)+" Transferred into savings"
    send_text = 'https://api.telegram.org/bot' + telegramBotToken + '/sendMessage?chat_id=' + telegramChatID + '&text=' + bot_message
    response = requests.get(send_text)
    
    return {
        'statusCode': 200,
        'body': json.dumps('Success')
    } 
